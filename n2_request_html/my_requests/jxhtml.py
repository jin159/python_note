from lxml import etree
import requests
import traceback
import MySQLdb
import time

def get_html(url):
    try:
        response = requests.request('get', url)
        fund_xpath = etree.HTML(response.text)
        fund_name = fund_xpath.xpath('//*[@id="qt_fund"]/span[1]/text()')[0][7:]
        fund_price = fund_xpath.xpath('//*[@id="qt_base"]/ul[1]/li[2]/span/text()')[0]
        date = [fund_name, fund_price]
        date.append(fund_xpath.xpath('//*[@id="qt_base"]/ul[3]/li[1]/span/text()')[0])
        date.append(fund_xpath.xpath('//*[@id="qt_base"]/ul[3]/li[7]/span/text()')[0])
        date.append(fund_xpath.xpath('//*[@id="qt_base"]/ul[3]/li[8]/span/text()')[0])
        date.append(url)
    except Exception  as ex:
        traceback.print_exc()
    return date


if __name__ == "__main__":
    # get_html("http://fundsuggest.eastmoney.com/FundSearch/api/FundSearchAPI.ashx?callback=jQuery18307677945248576012_1593525041010&m=1&" +
    #     "key=486001&_=1593525075353")
    fund_codes = "519674,001668,161031,001595,001548,001178,020003,540012,005911,270042,005918,162213,519069,040046,100039,166002,162703,260108,003376,001630,001878,000248,16022,519736,519688,110022,000601,001714,000045,162605,001632,004042,260101,004744,003766,180012,161716,206018,270044,001061,000406,001938,000083,005224,001102,000342,002079,161725,00962,519732,501058,519772,162412,206009,161028,110011,004424,004075,968061"\
        .split(',')
    for code in fund_codes:
        url='http://fund.10jqka.com.cn/myfund.php?userid=529791837&do=add&codes='+code+'&_='+str(time.time())
        response = requests.request('get', url)
        fund_xpath = etree.HTML(response.text)
    # # 打开数据库连接
    # # db = MySQLdb.connect("localhost", "testuser", "test123", "TESTDB", charset='utf8')
    # for key in fund_codes:
    #     get_html("http://fundsuggest.eastmoney.com/FundSearch/api/FundSearchAPI.ashx?callback=jQuery18307677945248576012_1593525041010&m=1&" +
    #     "key=" + key + "&_=1593525075353")
