# coding=utf-8
'''
Created on 2017年7月1日
@author: Jin
 
'''
#生成列表
print([x * x for x in range(1, 11)])


print([x*(x+1) for x in range(1,100,2)])


#复杂表达式
d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59 }
tds = ['<tr><td>%s</td><td>%s</td></tr>' % (name, score) for name, score in d.iteritems()]
print ('<table>')
print ('<tr><th>Name</th><th>Score</th><tr>')
print ('\n'.join(tds))
print ('</table>')
#字符串可以通过 % 进行格式化，用指定的参数替代 %s。字符串的join()方法可以把一个 list 拼接成一个字符串。

d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59 }
def generate_tr(name, score):
    if score<60:
        return '<tr><td>%s</td><td style="color:red"> %s</td></tr>' % (name, score)
    return '<tr><td>%s</td><td>%s</td></tr>' % (name, score)

tds = [generate_tr(name,score) for name, score in d.iteritems()]
print ('<table border="1">')
print ('<tr><th>Name</th><th>Score</th><tr>')
print ('\n'.join(tds))
print ('</table>')

#条件过滤

print ([x * x for x in range(1, 11) if x % 2 == 0])
#[4, 16, 36, 64, 100]


def toUppers(L):
    return [ x.upper() for x in L if isinstance(x, str)]

print (toUppers(['Hello', 'world', 101]))


#多层表达式
L = []
for m in 'ABC':
    for n in '123':
        L.append(m + n)


print ([ x*100+y*10+z for x in range(1,10) for y in range(10) for z in range(10) if x==z])
         
