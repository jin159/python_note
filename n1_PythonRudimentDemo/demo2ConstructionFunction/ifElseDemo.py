# coding=utf-8
'''
Created on 2017年6月27日
@author: Jin
 
'''
'''
注意: Python代码的缩进规则。具有相同缩进的代码被视为代码块，
上面的3，4行 print (语句就构成一个代码块（但不包括第5行的print）。
如果 if 语句判断为 True，就会执行这个代码块。

缩进请严格按照Python的习惯写法：4个空格，不要使用Tab，
更不要混合Tab和空格，否则很容易造成因为缩进引起的语法错误。
'''
age = 20
if age >= 18:
    print ('your age is', age)
    print ('adult')
print ('END')

print ('-------------------------')
#if ...else

age = 5
if age >= 18:
    print ('adult')
else:
    print ('teenager')
    
print ('----------------------------')
#if...elseif...else...    
if age >= 18:
    print ('adult')
else:
    if age >= 6:
        print ('teenager')
    else:
        print ('kid')



