# coding=utf-8
'''
Created on 2017年6月27日

@author: Jin
'''
#dict 相当于Map  注意: 一个 key-value 算一个，因此，dict大小为3。
'''
dict的第一个特点是查找速度快，
无论dict有10个元素还是10万个元素，查找速度都一样。
而list的查找速度随着元素增加而逐渐下降。

不过dict的查找速度快不是没有代价的，dict的缺点是占用内存大，还会浪费很多内容，
list正好相反，占用内存小，但是查找速度慢。

dict的第二个特点就是存储的key-value序对是没有顺序的！这和list不一样：

dict的第三个特点是作为 key 的元素必须不可变
'''
d = {
    'Adam': 95,
    'Lisa': 85,
    'Bart': 59
}
print (len(d)) #长度
print (d['Adam']) #95
#print (d['Paul'])  #KeyError: 'Paul'

#先判断一下 key 是否存在，用 in 操作符：
if 'Paul' in d:
    print (d['Paul'])
#是使用dict本身提供的一个 get 方法，在Key不存在的时候，返回None：
print (d.get('Bart'))
print (d.get('Paul'))

#更新dict 增加
print (d)
d['Paul'] = 72
print (d)
d['Adam'] = 72
print (d)


#遍历
for key in d:
    print (key + ':', d[key])
