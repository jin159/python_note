# coding=utf-8
'''
Created on 2017年6月27日
@author: Jin
 
'''
#定义函数
def my_abs(x):
    if x >= 0:
        return x
    else:
        return -x
#调用函数
print (my_abs(-100))
#如果没有return语句，函数执行完毕后也会返回结果，只是结果为 None。


#函数之返回多值
import math
def move(x, y, step, angle):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx, ny

x, y = move(100, 100, 60, math.pi / 6)
print (x, y)

#递归函数

def fact(n):
    if n==1:
        return 1
    return n * fact(n - 1)
print (fact(100))

#定义默认参数
def power(x, n=2):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s

print (power(2))
print (power(2,3))

#定义可变参数
def average(*args):
    sum =0.0
    if len(args) == 0:
        return sum
    for x in args:
        sum = sum + x
    return sum / len(args)
print (average())
print (average(1, 2))
print (average(1, 2, 2, 3, 4))






