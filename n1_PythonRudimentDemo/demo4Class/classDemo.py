# coding=utf-8
'''
Created on 2017年7月2日
@author: Jin
 
'''
'''
xx：公开属性
_xx:私有属性
__xx:局部私有属性
dir(object):查看类object属性
'''
#常用定义方法   1
class Example(Object):
    def add(self):
        pass
    def _minus(self):
        pass
    def __multiply(self):
        pass

'''
函数与方法的不同
方法也是类的一个属性
@classmethod  调用的时候用类名，而不是某个具体的对象
@property 像调用属性一样调用方法（不加括号）
'''
'''
函数——通过直接调用函数名，实现函数的调用；一串代码，实现调用
方法——必须和对象结合在一起使用，方法从属于某个类，由别的程序来调用
是否依附于某个类，是函数与方法的不同所在
类的方法也是类的属性
'''






