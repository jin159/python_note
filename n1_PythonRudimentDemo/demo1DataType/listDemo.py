# coding=utf-8
'''
Created on 2017年6月27日
@author: Jin
'''
L = ['Michael', 'Bob', 'Tracy']

print (L)

print (L[1])

#print (L[3]  # list index out of range

#倒序访问list
print (L[-1])
#print (L[-4]  # list index out of range

print ('List添加新元素')
#append()总是把新的元素添加到 list 的尾部。
L.append('Paul')
print (L)

'''
方法是用list的 insert()方法，它接受两个参数，第一个参数是索引号，第二个参数是待添加的新元素：
'''
L = ['Adam', 'Lisa', 'Bart']
L.insert(1, 'Paul')
print (L)
print ('List删除')
#用list的pop()方法删除 删除最后面的元素
L.pop()
print (L)

L.pop(2)
print (L)

#替换
L =['Adam', 'Lisa', 'Bart']
L[1] = 'Paul'
print (L)
