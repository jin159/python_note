# coding=utf-8
'''
Created on 2017年6月26日
 
@author: Jin
'''
"""
一、整数

Python可以处理任意大小的整数，当然包括负整数，
在Python程序中，整数的表示方法和数学上的写法一模一样，例如：1，100，-8080，0，等等。
计算机由于使用二进制，所以，有时候用十六进制表示整数比较方便，
十六进制用0x前缀和0-9，a-f表示，例如：0xff00，0xa5b4c3d2，等等。
"""
print (45678 + 0x12fd2)

"""
二、浮点数

浮点数也就是小数，之所以称为浮点数，是因为按照科学记数法表示时，一个浮点数的小数点位置是可变的，

"""
print (3.5/3)
"""
三、字符串

字符串是以''或""括起来的任意文本，比如'abc'，"xyz"等等。请注意，''或""本身只是一种表示方式，不是字符串的一部分，因此，字符串'abc'只有a，b，c这3个字符。
"""
print ('hello, world')
print ("hello, world")
"""
四、布尔值

布尔值和布尔代数的表示完全一致，一个布尔值只有True、False两种值，要么是True，要么是False，在Python中，可以直接用True、False表示布尔值（请注意大小写），也可以通过布尔运算计算出来。

布尔值可以用and、or和not运算。
"""
print (1> 1)
print (0xff == 255)
"""
五、空值

空值是Python里一个特殊的值，用None表示。None不能理解为0，因为0是有意义的，而None是一个特殊的空值。
"""








