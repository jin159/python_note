# coding=utf-8
'''
Created on 2017年7月1日
@author: Jin
 
'''

for i in range(1,101):
    if i % 7 == 0:
        print (i)

#索引迭代
L = ['Adam', 'Lisa', 'Bart', 'Paul']
for index, name in enumerate(L):
    print (index, '-', name)

L = ['Adam', 'Lisa', 'Bart', 'Paul']
for index, name in zip(range(1,len(L)+1),L):
    print (index, '-', name)

#迭代dict的value 


d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59 }
print (d.values())
# [85, 95, 59]
for v in d.values():
    print (v)

'''
dict除了values()方法外，还有一个 itervalues() 方法，用 itervalues() 方法替代 values() 方法，迭代效果完全一样：
'''
print('------------------------------------')
d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59 }
print (d.itervalues())
# #<dictionary-valueiterator object at 0x106adbb50>
for v in d.itervalues():
    print (v)

'''
1. values() 方法实际上把一个 dict 转换成了包含 value 的list。

2. 但是 itervalues() 方法不会转换，它会在迭代过程中依次从 dict 中取出 value，所以 itervalues() 方法比 values() 方法节省了生成 list 所需的内存。

3. 打印 itervalues() 发现它返回一个 <dictionary-valueiterator> 对象，这说明在Python中，for 循环可作用的迭代对象远不止 list，tuple，str，unicode，dict等，任何可迭代对象都可以作用于for循环，而内部如何迭代我们通常并不用关心。
'''

d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59, 'Paul': 74 }
sum = 0.0
for v in d.itervalues():
    sum = sum + v
print (sum / len(d))

#迭代dict的key和value
d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59 }
print (d.items())

d = { 'Adam': 95, 'Lisa': 85, 'Bart': 59, 'Paul': 74 }

sum = 0.0
for k, v in d.items():
    sum = sum + v
    print (k,':',v)
print ('average', ':', sum/len(d))
