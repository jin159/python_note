# coding=utf-8
'''
Created on 2021-04-20 17:15
@author: jin
'''
import math

import matplotlib as mpl
import matplotlib.pyplot as plt
import mpl_finance as mpf
import pandas as pd
import numpy as np
import talib as tl
from datetime import datetime

def set_sma(df):
    sma_10 = tl.SMA(np.array(df['close']), 10)
    sma_30 = tl.SMA(np.array(df['close']), 30)
    sma_100 = tl.SMA(np.array(df['close']), 100)
    sma_150 = tl.SMA(np.array(df['close']), 150)
    df['sma_10']=sma_10;
    df['sma_30']=sma_30;
    df['sma_100']=sma_100;
    df['sma_150']=sma_150;
    # df = pd.DataFrame(df, columns=rs.fields)
    df['date'] = pd.to_datetime(df['date'])
    print(df.info)
    # print(df[100:120])
    #### 结果集输出到csv文件 ####
    df.to_csv(".\\data\\tushare1_159915.csv", index=False)
def getMoney(df,date,monSum):
    #方法一  使用行索引切片，['2019/9/1':'2019/9/30']，缺点是要求日期必须是连续的。为了方便查看取前5条，以下其他方法均取前5条，由于未进行排序，顺序会有差异
    df.set_index('date',inplace=True)
    df1 = df[date:'2021-04-10']
    close_mon=df1.loc[date,'close']

    fr =math.floor((monSum/2/close_mon)/100)*100
    print(fr)
    monSy = monSum - fr*close_mon*1.001
    print(monSy)
    for row in df1:
        rate = ((row.high+row.low)/2-row.sma_100)/row.sma_100
        print(rate)
monSum =10000
df1 =pd.read_csv(".\\data\\tushare_159915.csv",encoding='utf-8') #注意中文格式
close_mon=df1.loc['2013-01-30','close']
fr =math.floor((monSum/2/close_mon)/100)*100
monSy = monSum - fr*close_mon*1.001
count=0
def mc(row,fr,shou=1000):
    global count,mon
    global monSy
    if fr<= 0 :
        return fr
    close_mon =row.low
    fr_j =math.floor((shou/close_mon)/100);
    if fr -fr_j <0:
        return fr
    fr= fr-fr_j;
    monSy=monSy+fr_j*close_mon*0.999*100
    count=count+1;
    mon =mon+fr_j*close_mon*0.001*100
    return fr;
def mr(row,fr,shou=1000):
    global count,mon
    global monSy
    close_mon =row.low
    fr_add =math.floor((shou/close_mon)/100);
    if monSy-fr_add*close_mon*1.001*100 <0:
        return fr
    fr=fr+fr_add;
    monSy=monSy-fr_add*close_mon*1.001*100
    count=count+1;
    mon =mon+fr_add*close_mon*0.001*100
    return fr;
monysyArr=[]
monyArr=[]
frArr=[]
rateArr=[]
# 策略1
def cl1():
    monSum =10000
    close_mon=df1.loc['2013-01-30','close']
    fr =math.floor((monSum/2/close_mon)/100)*100
    monSy = monSum - fr*close_mon*1.001

    monysyArr=[]
    monyArr=[]
    frArr=[]
    rateArr=[]

    for i,row in df1.iterrows():
        r = ((row.high+row.low)/2-row.sma_30)/row.sma_30
        close_mon =row.low
        if r>0.05 and  (fr*close_mon)>3000:
            fr = mc(row,fr)
        if r>0.1 and fr*close_mon>2000:
            fr = mc(row,fr)
        if r>0.15 and fr*close_mon>1000:
            fr = mc(row,fr)
        if r>0.2 :
            monSy=monSy+fr*close_mon*0.999;
            fr = 0;
        if r<0.15 and fr == 0 :
            fr = mr(row,fr)
        if r<0.1 and monSy>9000:
            fr = mr(row,fr)
        if r<0.05 and monSy>7000:
            fr = mr(row,fr)
        if r<0 and monSy>5000:
            fr = mr(row,fr)
        if r<-0.05 and monSy>3000:
            fr = mr(row,fr)
        if r<-0.1 and monSy>2000:
            fr = mr(row,fr)
        if r<-0.15 and monSy>1000:
            fr = mr(row,fr)
        if r<-0.2 and monSy>0:
            fr_add =math.floor((monSy/close_mon)/100)*100;
            fr=fr+fr_add;
            monSy=monSy-fr_add*close_mon*1.001
        monysyArr.append(monSy)
        monyArr.append(monSy+fr*close_mon)
        frArr.append(fr)
        rateArr.append(r)
    df1['monysy']=monysyArr;
    df1['monySum']=monyArr;
    df1['fr']=frArr;
    df1['rate']=rateArr;
    print(df1)
    df1.to_csv(".\\data\\test30_159915.csv", index=False)

if __name__ == '__main__':
    df=pd.read_csv(".\\data\\tushare_159915.csv",encoding='utf-8') #注意中文格式
    set_sma(df)

    getMoney(df,'2012-01-30',10000)

#策略2 最优解 目前
for i,row in df1.iterrows():
    r = ((row.high+row.low)/2-row.sma_100)/row.sma_100
    close_mon =row.low
    sum_ = monSy+fr*close_mon
    if r>-0.01 and r<0.3:
        if (fr*close_mon)<= 40000:
            mr(row)
        if (fr*close_mon)> 6000:
            fr = mc(row,fr)
    if r>0.3 and (fr*close_mon)<= 6000:
        fr = mc(row,fr)
    if r<-0.01 and (fr*close_mon)<= 6000:
        mr(row)
    if r<-0.15 and (fr*close_mon)<= 7500:
        mr(row)
    if r<-0.20 and (fr*close_mon)<= 8500:
        mr(row)
    if r<-0.30 and (fr*close_mon)<= 9500:
        mr(row)
    if r<-0.35 :
        fr_add =math.floor((monSy/close_mon)/100)*100;
        fr=fr+fr_add;
        monSy=monSy-fr_add*close_mon*1.001
    if r>0.05 and  (fr*close_mon)> 5000:
        fr = mc(row,fr)
    if r>0.1 and  (fr*close_mon)> 4500:
        fr = mc(row,fr)
    if r>0.20 and  (fr*close_mon)> 3500:
        fr = mc(row,fr)
    if r>0.35 and  (fr*close_mon)> 2500:
        fr = mc(row,fr)
    if r>0.40 and  (fr*close_mon)> 1500:
        fr = mc(row,fr)
    if r>0.45 :
        monSy=monSy+fr*close_mon*0.999;
        fr = 0;
    monysyArr.append(monSy)
    monyArr.append(monSy+fr*close_mon)
    frArr.append(fr)
    rateArr.append(r)
#     print(r)
# print(monysyArr)
# print(monyArr)
# print(frArr)
# print(rateArr)
df1['monysy']=monysyArr;
df1['monySum']=monyArr;
df1['fr']=frArr;
df1['rate']=rateArr;
print(df1)
df1.to_csv(".\\data\\test30_2_159915(count="+str(count)+").csv", index=False)

#策略3
#策略4
for i,row in df1.iterrows():
    r = ((row.high+row.low)/2-row.sma_100)/row.sma_100
    close_mon =row.low
    sum_ = monSy+fr*close_mon
    if r>0.00 and  (fr*close_mon)>sum_*0.9:
        fr = mc(row,fr)
    if r>0.02 and  (fr*close_mon)>sum_*0.8:
        fr = mc(row,fr)
    if r>0.08 and  (fr*close_mon)>sum_*0.6:
        fr = mc(row,fr)
    if r>0.15 and fr*close_mon>sum_*0.3:
        fr = mc(row,fr)
    if r>0.25 and fr*close_mon>sum_*0.2:
        fr = mc(row,fr)
    if r>0.40 :
        monSy=monSy+fr*close_mon*0.999;
        fr = 0;
    if r<0.20 and fr == 0 :
        mr(row)
    if r<0.10 and monSy>sum_*0.9:
        mr(row)
    if r<0.03 and monSy>sum_*0.7:
        mr(row)
    if r<0 and monSy>sum_*0.5:
        mr(row)
    if r<-0.05 and monSy>sum_*0.4:
        mr(row)
    if r<-0.1 and monSy>sum_*0.3:
        mr(row)
    if r<-0.15 and monSy>sum_*0.2:
        mr(row)
    if r<-0.2 and monSy>0:
        fr_add =math.floor((monSy/close_mon)/100)*100;
        fr=fr+fr_add;
        monSy=monSy-fr_add*close_mon*1.001
    monysyArr.append(monSy)
    monyArr.append(monSy+fr*close_mon)
    frArr.append(fr)
    rateArr.append(r)


def cl(df1):
    close_mon=df1['close'].get(0)
    fr =math.floor((monSum/2/close_mon)/100)*100
    monSy = monSum - fr*close_mon*1.001
    count=0
    monysyArr=[]
    monyArr=[]
    frArr=[]
    rateArr=[]
    for i,row in df1.iterrows():
        r = ((row.high+row.low)/2-row.sma_150)/row.sma_150
        close_mon =row.low
        sum_ = monSy+fr*close_mon
        if r>-0.01 and r<0.3:
            if (fr*close_mon)<= 40000:
                fr = mr(row,fr)
            if (fr*close_mon)> 6000:
                fr = mc(row,fr)
        if r>0.3 and (fr*close_mon)<= 6000:
            fr = mc(row,fr)
        if r<-0.01 and (fr*close_mon)<= 6000:
            fr = mr(row,fr)
        if r<-0.15 and (fr*close_mon)<= 7500:
            fr = mr(row,fr)
        if r<-0.20 and (fr*close_mon)<= 8500:
            fr = mr(row,fr)
        if r<-0.30 and (fr*close_mon)<= 9500:
            fr = mr(row,fr)
        if r<-0.35 :
            fr_add =math.floor((monSy/close_mon)/100)*100;
            fr=fr+fr_add;
            monSy=monSy-fr_add*close_mon*1.001
        if r>0.05 and  (fr*close_mon)> 5000:
            fr = mc(row,fr)
        if r>0.1 and  (fr*close_mon)> 4500:
            fr = mc(row,fr)
        if r>0.20 and  (fr*close_mon)> 3500 and fr>0:
            fr = mc(row,fr)
        if r>0.35 and  (fr*close_mon)> 2500 and fr>0:
            fr = mc(row,fr)
        if r>0.40 and  (fr*close_mon)> 1500 and fr>0:
            fr = mc(row,fr)
        if r>0.45 and fr>0:
            monSy=monSy+fr*close_mon*0.999;
            fr = 0;
        monysyArr.append(monSy)
        monyArr.append(monSy+fr*close_mon)
        frArr.append(fr)
        rateArr.append(r)
    #     print(r)
    # print(monysyArr)
    # print(monyArr)
    # print(frArr)
    # print(rateArr)
    df1['monysy']=monysyArr;
    df1['monySum']=monyArr;
    df1['fr']=frArr;
    df1['rate']=rateArr;
    # print(df1)
    df1.to_csv(".\\data\\exel\\159920_恒生_150(count="+str(count)+").csv", index=False)

 #策略2
def cl2(df1):
    global count,monSum,mon,monSy
    close_mon=df1['close'].get(0)
    fr =math.floor((monSum/2/close_mon)/100)*100
    monSy = monSum - fr*close_mon*1.001
    monysyArr=[]
    monyArr=[]
    frArr=[]
    rateArr=[]
    for i,row in df1.iterrows():
        r = ((row.high+row.low)/2-row.sma_150)/row.sma_150
        close_mon =row.low
        sum_ = monSy+fr*close_mon
        if r>0.00 and r<0.05:
            if (fr*close_mon)<= 40000:
                fr = mr(row,fr)
            if (fr*close_mon)> 6000:
                fr = mc(row,fr)
        if r<0.00 and (fr*close_mon)<= 5000:
            fr = mr(row,fr)
        if r<-0.05 and (fr*close_mon)<= 6500:
            fr = mr(row,fr)
        if r<-0.10 and (fr*close_mon)<= 7500:
#             print( (fr*close_mon),r)
            fr = mr(row,fr)
        if r<-0.15 and (fr*close_mon)<= 8500:
            fr = mr(row,fr)
        if r<-0.20 and (fr*close_mon)<= 9500:
            fr = mr(row,fr)
        if r<-0.35 :
            fr_add =math.floor((monSy/close_mon)/100)*100;
            fr=fr+fr_add;
            monSy=monSy-fr_add*close_mon*1.001
            mon=mon++fr_add*close_mon*0.001
#         if (fr*close_mon)<= 10000 and  r>-0.30:
#             fr = mc(row,fr)
#         if (fr*close_mon)<= 9000 and  r>-0.15:
#             fr = mc(row,fr)
#         if (fr*close_mon)<= 8000 and  r>-0.05:
#             fr = mc(row,fr)

        if r>0.05 and  (fr*close_mon)> 5500:
            fr = mc(row,fr)
        if r>0.1 and  (fr*close_mon)> 4500:
            fr = mc(row,fr)
        if r>0.20 and  (fr*close_mon)> 3500 and fr>0:
            fr = mc(row,fr)
        if r>0.35 and  (fr*close_mon)> 2500 and fr>0:
            fr = mc(row,fr)
        if r>0.45 and  (fr*close_mon)> 1500 and fr>0:
            fr = mc(row,fr)

#         if(fr*close_mon)< 1000 and r<0.30:
#             fr = mr(row,fr)
#         if(fr*close_mon)< 2000 and r<0.15:
#             fr = mr(row,fr)
#         if(fr*close_mon)< 3500 and r<0.5:
#             fr = mr(row,fr)

#         if r>0.45 and fr>0:
#             monSy=monSy+fr*close_mon*0.999;
#             fr = 0;
        monysyArr.append(monSy)
        monyArr.append(monSy+fr*close_mon)
        frArr.append(fr)
        rateArr.append(r)
    #     print(r)
    # print(monysyArr)
    # print(monyArr)
    # print(frArr)
    # print(rateArr)
    df1['monysy']=monysyArr;
    df1['monySum']=monyArr;
    df1['fr']=frArr;
    df1['rate']=rateArr;
    # print(df1)
    df1.to_csv(".\\data\\exel\\159915_创业板-150(count="+str(count)+")"+str(mon)+".csv", index=False)
    print(mon)




def cl2(df1):
    global count,monSum,mon,monSy
    close_mon=df1['close'].get(0)
    fr =math.floor((monSum/2/close_mon)/100)
    monSy = monSum - fr*close_mon*1.001*100
    monysyArr=[]
    monyArr=[]
    frArr=[]
    rateArr=[]
    for i,row in df1.iterrows():
        r = ((row.high+row.low)/2-row.sma_150)/row.sma_150
        close_mon =row.low
        sum_ = monSy+fr*close_mon*100
        if r>0.00 and r<0.05:
            if (fr*close_mon*100)<= 4000:
                fr = mr(row,fr)
            if (fr*close_mon*100)> 6000:
                fr = mc(row,fr)
        if r<0.00 and (fr*close_mon*100)<= 5000:
            fr = mr(row,fr)
        if r<-0.05 and (fr*close_mon*100)<= 6500:
            fr = mr(row,fr)
        if r<-0.10 and (fr*close_mon*100)<= 7500:
#             print( (fr*close_mon),r)
            fr = mr(row,fr)
        if r<-0.15 and (fr*close_mon*100)<= 8500:
            fr = mr(row,fr)
        if r<-0.20 and (fr*close_mon*100)<= 9500:
            fr = mr(row,fr)
        if r<-0.35 :
            fr_add =math.floor((monSy/close_mon)/100);
            fr=fr+fr_add;
            monSy=monSy-fr_add*close_mon*1.001*100
            mon=mon+fr_add*close_mon*0.001*100

        if r>0.08 and  (fr*close_mon*100)> 5500:
            fr = mc(row,fr)
        if r>0.1 and  (fr*close_mon*100)> 4500:
            fr = mc(row,fr)
        if r>0.20 and  (fr*close_mon*100)> 3500 and fr>0:
            fr = mc(row,fr)
        if r>0.35 and  (fr*close_mon*100)> 2500 and fr>0:
            fr = mc(row,fr)
        if r>0.45 and  (fr*close_mon*100)> 1500 and fr>0:
            fr = mc(row,fr)
        monysyArr.append(monSy)
        monyArr.append(monSy+fr*close_mon*100)
        frArr.append(fr)
        rateArr.append(r)
    #     print(r)
    # print(monysyArr)
    # print(monyArr)
    # print(frArr)
    # print(rateArr)
    df1['monysy']=monysyArr;
    df1['monySum']=monyArr;
    df1['fr']=frArr;
    df1['rate']=rateArr;
    # print(df1)
    df1.to_csv(".\\data\\exel\\159915_创业板-150(count="+str(count)+")"+str(mon)+".csv", index=False)
    print(mon)#         if r>0.00 and r<0.05:
#             fr = mr(row,fr,1)
#         if r<0.00 and r>-0.05 and (fr*close_mon)<= 5000:
#             fr = mr(row,fr,200)
#         if r<-0.05 and r>= -0.1 and (fr*close_mon)<= 6000:
#             fr = mr(row,fr,400)
#         if r<-0.10 and r>=-0.150 and (fr*close_mon)<= 7000:
#             fr = mr(row,fr,800)
#         if r<-0.15 and r>=-2 and (fr*close_mon)<= 8000:
#             fr = mr(row,fr,1500)
#         if r<-0.20 and r>=-0.35 and (fr*close_mon)<= 9000:
#             fr = mr(row,fr,3000)
#         if r<-0.35 :
#             fr_add =math.floor((monSy/close_mon)/100)*100;
#             fr=fr+fr_add;
#             monSy=monSy-fr_add*close_mon*1.001
#             mon=mon++fr_add*close_mon*0.001
#         if r>0.05 and  r<=0.1 and (fr*close_mon)>5000:
#             fr = mc(row,fr,200)
#         if r>0.1 and  r<=0.2  and (fr*close_mon)>4000:
#             fr = mc(row,fr,4000)
#         if r>0.20 and r<=0.35 and (fr*close_mon)>3000:
#             fr = mc(row,fr,800)
#         if r>0.35 and  r<=0.45 and (fr*close_mon)>2000:
#             fr = mc(row,fr,1500)
#         if r>0.45 and  fr>0 and (fr*close_mon)>1000:
#             fr = mc(row,fr,1000)


#         if(fr*close_mon)< 1000 and r<0.30:
#             fr = mr(row,fr)
#         if(fr*close_mon)< 2000 and r<0.15:
#             fr = mr(row,fr)
#         if(fr*close_mon)< 3500 and r<0.5:
#             fr = mr(row,fr)

#         if r>0.45 and fr>0:
#             monSy=monSy+fr*close_mon*0.999;
#             fr = 0;
