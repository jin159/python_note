# coding=utf-8
'''
Created on 2017年7月2日
@author: Jin
 
'''

#定义类的继承

#class DeriveClassName(BaseClassName):
'''
class A(Object):
    def method(selfself,arg):
        pass
    class B(A):
        def method(self,arg):
            super(B,self).method(arg)
'''

'''
一个子类的对象，在判断类型的时候，会被认为是自己所在的类，同时会被认为是它的一个父类
内建函数：isinstance——判断该对象的类型时是否会被认为是它的父类
issubclass——判断是否是子类
'''
















   