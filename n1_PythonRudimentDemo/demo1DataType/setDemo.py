# coding=utf-8
'''
Created on 2017年6月27日
@author: Jin
 
'''
'''
set 持有一系列元素，这一点和 list 很像，但是set的元素没有重复，
而且是无序的，这点和 dict 的 key很像。

set的内部结构和dict很像，唯一区别是不存储value，因此，判断一个元素是否在set中速度很快。

set存储的元素和dict的key类似，必须是不变对象，因此，任何可变对象是不能放入set中的。

最后，set存储的元素也是没有顺序的。

'''

s = set(['A', 'B', 'C', 'C'])
print (s)
print (len(s))


s = set(['Adam', 'Lisa', 'Bart', 'Paul'])
#访问 set中的某个元素实际上就是判断一个元素是否在set中。
print ('Bart' in s)

#遍历
for name in s:
    print (name)

#更新
s.add(4)
print (s)
s.add(4)
print (s) # 不会改变也不报错

#删除set中的元素
s.remove(4)
print (s)

#s.remove(4)#如果删除的元素不存在set中，remove()会报错：


