# coding=utf-8
'''
Created on 2017年6月27日
@author: Jin
 
'''

'''
tuple是另一种有序的列表，中文翻译为“ 元组 ”。tuple 和 list 非常类似，但是，tuple一旦创建完毕，就不能修改了。
'''
t = ('Adam', 'Lisa', 'Bart')
print (t)
#包含 0 个元素的 tuple，也就是空tuple，直接用 ()表示：
t=()
print (t)
# t = (1) 用()定义单元素的tuple有歧义，所以 Python 规定，单元素 tuple 要多加一个逗号“,”，这样就避免了歧义：
t = (1,)
print (t)
t =(1,2,3,)
print (t)
#“可变”的tuple
t = ('a', 'b', ['A', 'B'])
print (t)

