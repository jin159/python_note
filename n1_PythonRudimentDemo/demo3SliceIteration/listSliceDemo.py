# coding=utf-8
'''
Created on 2017年6月26日
 
@author: Jin
'''
"""
请利用切片，取出：

1. 前10个数；
2. 3的倍数；
3. 不大于50的5的倍数。
"""
L = range(1, 101)

print (L[:10])
print (L[2::3])
print (L[4:50:5])


#倒序切片
L = ['Adam', 'Lisa', 'Bart', 'Paul']
L[-2:]     #['Bart', 'Paul']

L[:-2]     #['Adam', 'Lisa']

L[-3:-1]   #['Lisa', 'Bart']

L[-4:-1:2] #['Adam', 'Bart']

L = range(1, 101)
print (L[-10:])     #[91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
print (L[:-1:5] )   #[1, 6, 11, 16, 21, 26, 31, 36, 41, 46, 51, 56, 61, 66, 71, 76, 81, 86, 91, 96]


#字符串进行切片
'ABCDEFG'[:3]    #'ABC'
'ABCDEFG'[::2]   #'ACEG'


